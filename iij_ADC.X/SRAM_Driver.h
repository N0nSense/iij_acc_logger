/* 
 * File:   SRAM_Driver.h
 * Author: iijima
 *
 * Created on May 30, 2019, 3:20 PM
 */

#ifndef SRAM_DRIVER_H
#define	SRAM_DRIVER_H

#include <stdint.h>
#include "mcc_generated_files/spi1.h"

//#define SpiExchangeByte SPI1_Exchange8bit

#ifdef	__cplusplus
extern "C" {
#endif
    
    uint8_t (*SpiExchangeByte)(uint8_t);

    extern volatile uint16_t _current_addr;
    
    void Sram_SetSpiExchangeByteFunction( uint8_t(*func)(uint8_t) );
    
    uint8_t Sram_Init();
    
    uint8_t Sram_Init1();
    uint8_t Sram_Init2();
    
    uint16_t Sram_GetLastActionAddress();
    void Sram_SetAddress(uint16_t addr);
    
    uint8_t Sram_ReadByte(uint16_t addr);
    
    void Sram_Read(uint8_t *buf, uint16_t len, uint16_t addr);
    uint8_t Sram_ReadByteNextAddress();
    
    void Sram_WriteByte(uint8_t value, uint16_t addr);
    void Sram_WriteByteNextAddress(uint8_t value);
    
    void Sram_Write(uint8_t *buf, uint16_t len, uint16_t addr);
    

    
    

#ifdef	__cplusplus
}
#endif

#endif	/* SRAM_DRIVER_H */

