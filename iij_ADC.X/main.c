/**
  Generated Main Source File

  Company:
    Microchip Technology Inc.

  File Name:
    main.c

  Summary:
    This is the main file generated using PIC10 / PIC12 / PIC16 / PIC18 MCUs

  Description:
    This header file provides implementations for driver APIs for all modules selected in the GUI.
    Generation Information :
        Product Revision  :  PIC10 / PIC12 / PIC16 / PIC18 MCUs - 1.76
        Device            :  PIC16F19155
        Driver Version    :  2.00
*/

/*
    (c) 2018 Microchip Technology Inc. and its subsidiaries. 
    
    Subject to your compliance with these terms, you may use Microchip software and any 
    derivatives exclusively with Microchip products. It is your responsibility to comply with third party 
    license terms applicable to your use of third party software (including open source software) that 
    may accompany Microchip software.
    
    THIS SOFTWARE IS SUPPLIED BY MICROCHIP "AS IS". NO WARRANTIES, WHETHER 
    EXPRESS, IMPLIED OR STATUTORY, APPLY TO THIS SOFTWARE, INCLUDING ANY 
    IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY, AND FITNESS 
    FOR A PARTICULAR PURPOSE.
    
    IN NO EVENT WILL MICROCHIP BE LIABLE FOR ANY INDIRECT, SPECIAL, PUNITIVE, 
    INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, COST OR EXPENSE OF ANY KIND 
    WHATSOEVER RELATED TO THE SOFTWARE, HOWEVER CAUSED, EVEN IF MICROCHIP 
    HAS BEEN ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE. TO 
    THE FULLEST EXTENT ALLOWED BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL 
    CLAIMS IN ANY WAY RELATED TO THIS SOFTWARE WILL NOT EXCEED THE AMOUNT 
    OF FEES, IF ANY, THAT YOU HAVE PAID DIRECTLY TO MICROCHIP FOR THIS 
    SOFTWARE.
*/

#include "mcc_generated_files/mcc.h"
#include "SRAM_Driver.h"

uint16_t adcResult[6];
volatile uint8_t adcFlag=0;

void AdcProc(){
    adcFlag=1;
    return;
}


uint16_t tmptimer=0;
uint8_t volatile rxFlag=0;
uint8_t volatile rx2=0;

uint8_t volatile frameType=0;
uint8_t volatile frameIndex=0;
uint8_t volatile zigBeeRxFlag=0;
uint8_t volatile zigBeeTxError=0xFF;
uint16_t volatile frameLength=0;

uint8_t volatile writeFlag=0;

uint8_t volatile isAssociated = 0;

#define PACKET_BYTE 180
uint8_t volatile readFlag=0;
uint8_t txDat[PACKET_BYTE+2];
uint8_t rcvCmd=0;

uint16_t host_payload1;
uint16_t host_payload2;
uint8_t volatile host_receive_flag=0;

#define PACKET_NUM 200

const uint8_t zigBeeSendTemplateDefaultSum = 0x0E;
const uint8_t zigBeeSendTemplate[]=  {0x7E,0x00,0xC4,0x10,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFE,0x00,0x00};
const uint8_t zigBeeSendTemplate_s[]={0x7E,0x00,0x12,0x10,0x01,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0xFF,0xFE,0x00,0x00};    // identification

void RxInterrupt(){
    EUSART1_Receive_ISR();
    rxFlag=EUSART1_Read();
}

void Rx2Interrupt(){
    EUSART2_Receive_ISR();
    rx2=EUSART2_Read();
    if(zigBeeRxFlag&0x01){
        rx2^=0x20;
        zigBeeRxFlag&=0xFE;
        goto ESCAPED;
    }
    
    switch(rx2){
        case 0x7E:
            frameIndex=1;
            zigBeeRxFlag&=0xFD;
            frameType=0;
            goto UART2_RX_END;
            break;
        case 0x7D:
            zigBeeRxFlag|=0x01;
            goto UART2_RX_END;
        default:
            break;
    }
    
    ESCAPED:
    switch(frameIndex){
        case 0:
            goto UART2_RX_END;
        case 1:
            frameLength=((uint16_t)(rx2)<<8);
            break;
        case 2:
            frameLength+=rx2;
            frameLength++;
            zigBeeRxFlag|=0x02;
            break;
        case 3:
            frameType=rx2;
            break;
        case 4:
            if(frameType==0x8A && rx2==0x02){
                isAssociated = 1;
            }
            break;
        case 8:
            if(frameType==0x8B){
                zigBeeTxError=rx2;
            }
            break;
        case 15:
            if(frameType==0x90){
                host_payload1=((uint16_t)(rx2)<<8);
            }
            break;
        case 16:
            if(frameType==0x90){
                host_payload1+=rx2;
            }
            break;
        case 17:
            if(frameType==0x90){
                host_payload2=((uint16_t)(rx2)<<8);
            }
            break;
        case 18:
            if(frameType==0x90){
                host_payload2+=rx2;
                host_receive_flag=1;
            }
            break;
        default:
            break;
    }
    
    frameIndex++;
    if(frameLength==0 && zigBeeRxFlag&0x02){
        zigBeeRxFlag&=0xFD;
        frameIndex=0;
        goto UART2_RX_END;
    }
    if(zigBeeRxFlag&0x02){
        frameLength--;
    }
    UART2_RX_END:
    PIR3bits.RC2IF=0;
}


void ReadSram();

/*
                         Main application
 */
void main(void)
{
    static const uint8_t adcPinList[6]={0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D};
    
    
    // initialize the device
    SYSTEM_Initialize();
    SRAM_CS_PORT=1;
    TMR1_SetInterruptHandler(AdcProc);
    EUSART1_SetRxInterruptHandler(RxInterrupt);
    EUSART2_SetRxInterruptHandler(Rx2Interrupt);
    
    // When using interrupts, you need to set the Global and Peripheral Interrupt Enable bits
    // Use the following macros to:

    // Enable the Global Interrupts
    INTERRUPT_GlobalInterruptEnable();

    // Enable the Peripheral Interrupts
    INTERRUPT_PeripheralInterruptEnable();

    Sram_SetSpiExchangeByteFunction(SPI1_Exchange8bit);
    TMR1_StartTimer();
    for(uint8_t i;i<100;i++);
    SRAM_CS_PORT=0;
    Sram_Init1();
    SRAM_CS_PORT=1;
    for(uint8_t i;i<100;i++);
    SRAM_CS_PORT=0;
    Sram_Init2();
    SRAM_CS_PORT=1;
    
    rxFlag='X';
    
    
    while (1)
    {
        if(readFlag){
            ReadSram();
            readFlag=0;
        }
        
        
        if(adcFlag==1){
            for(uint8_t i=0; i<6; i++){
                adcResult[i] = ADCC_GetSingleConversion(adcPinList[i],300);
                if(writeFlag==0){
                    continue;
                }
            }
            if(!writeFlag)
                goto NO_WRITE;
            for(uint8_t i=1; i<6; i+=2){
                Sram_WriteByteNextAddress( (uint8_t)((adcResult[i-1]&0xff0)>>4) );
                Sram_WriteByteNextAddress( ((uint8_t)((adcResult[i-1]&0x00f)<<4) + (uint8_t)((adcResult[i]&0xf00)>>8)) ); 
                Sram_WriteByteNextAddress( (uint8_t)(adcResult[i]&0x0ff) );
            }
            if (Sram_GetLastActionAddress()>=0xFFF8){
                SRAM_CS_PORT=1;
                for(uint8_t i=0;i<10;i++);
                SRAM_CS_PORT=0;
                Sram_WriteByte(0,0xFFFF);
            }
            NO_WRITE:
            
            if(writeFlag){
                //printf("%03x  %03x  %03x  %03x  %03x  %03x   %04x w",adcResult[0],adcResult[1],adcResult[2],adcResult[3],adcResult[4],adcResult[5],_current_addr);
                //printf("%04X\n\r",_current_addr);
            } else {
                printf("%03x  %03x  %03x  %03x  %03x  %03x  n\n\r",adcResult[0],adcResult[1],adcResult[2],adcResult[3],adcResult[4],adcResult[5]);
            }
            //printf("\n\r");
            adcFlag=0;
        }
        
        switch(rxFlag){
            case 's':
                TMR1_StopTimer();
                rxFlag=0;
                break;
            case 'S':
                TMR1_StartTimer();
                rxFlag=0;
                break;
            case 'X':
                SRAM_CS_PORT=0;
                Sram_WriteByte(0,0xFFFF);
                writeFlag=1;
                rxFlag=0;
                break;
            case 'x':
                SRAM_CS_PORT=1;
                writeFlag=0;
                rxFlag=0;
                break;
            case 'R':
                rxFlag=0;
                readFlag=1;
                break;
                
            default:
                break;
        }
        if(rxFlag){
            rxFlag=0;
        }
        
    }
}

void SendRequest(){
    ZIGBEE_SENDREQ:
    host_receive_flag=0;
    printf("send request\n\r");
    for(uint8_t i=0;i<17;i++){
        EUSART2_Write(zigBeeSendTemplate_s[i]);
    }
    uint8_t txSum_s = zigBeeSendTemplateDefaultSum;
    EUSART2_Write(0xFF);
    txSum_s+=0xFF;
    EUSART2_Write(0xFE);
    txSum_s+=0xFE;
    EUSART2_Write(((PACKET_NUM)&0xFF00)>>8);
    txSum_s+=(((PACKET_NUM)&0xFF00)>>8);
    EUSART2_Write(PACKET_NUM&0x00FF);
    txSum_s+=(PACKET_NUM&0x00FF);
    txSum_s=0xFF-txSum_s;
    zigBeeTxError=0xFF;
    if(txSum_s==0x11 ||
        txSum_s==0x13 ||
        txSum_s==0x7D ||
        txSum_s==0x7E){
        EUSART2_Write(0x7D);
        EUSART2_Write(txSum_s^0x20);
    } else {
        EUSART2_Write(txSum_s);
    }
    printf("wait...\t");
    tmptimer=0;
    while(1){
        //uint8_t errorFlag=0;
        tmptimer++;
        if(tmptimer>=50000){
            printf("retry\n\r");
            goto ZIGBEE_SENDREQ;
        }
        if(zigBeeTxError!=0xFF){
            if(zigBeeTxError==0x00){
                //printf("");
                break;
            } else if(zigBeeTxError!=0xFE){
                printf("error %02x    ",zigBeeTxError);
                zigBeeTxError=0xFE;
            }
        }
        __delay_us(10);
    }
    printf("\n\rwait respons...");
    while(host_receive_flag==0){}
    printf("\n\r");
    printf("pay1: %04X\t",host_payload1);
    printf("pay2: %04X\n\r",host_payload2);
    
    
}

void SendFinish(){
    ZIGBEE_SENDFIN:
    printf("send finish\n\r");
    for(uint8_t i=0;i<17;i++){
        EUSART2_Write(zigBeeSendTemplate_s[i]);
    }
    uint8_t txSum_s = zigBeeSendTemplateDefaultSum;
    EUSART2_Write(0xFF);
    txSum_s+=0xFF;
    EUSART2_Write(0xF0);
    txSum_s+=0xF0;
    EUSART2_Write(((PACKET_NUM)&0xFF00)>>8);
    txSum_s+=(((PACKET_NUM)&0xFF00)>>8);
    EUSART2_Write(PACKET_NUM&0x00FF);
    txSum_s+=(PACKET_NUM&0x00FF);
    txSum_s=0xFF-txSum_s;
    zigBeeTxError=0xFF;
    if(txSum_s==0x11 ||
        txSum_s==0x13 ||
        txSum_s==0x7D ||
        txSum_s==0x7E){
        EUSART2_Write(0x7D);
        EUSART2_Write(txSum_s^0x20);
    } else {
        EUSART2_Write(txSum_s);
    }
    printf("wait...\t");
    tmptimer=0;
    while(1){
        //uint8_t errorFlag=0;
        tmptimer++;
        if(tmptimer>=50000){
            printf("retry\n\r");
            goto ZIGBEE_SENDFIN;
        }
        if(zigBeeTxError!=0xFF){
            if(zigBeeTxError==0x00){
                //printf("");
                break;
            } else if(zigBeeTxError!=0xFE){
                printf("error %02x    ",zigBeeTxError);
                zigBeeTxError=0xFE;
            }
        }
        __delay_us(10);
    }
    printf("success\n\r");
}

void ReadSram(){
    XBEE_SLEEP_SetLow();
    printf("wait wake up\n\r");
    while(isAssociated==0){}
    printf("associated\n\r");
    SRAM_CS_PORT=1;
    TMR1_StopTimer();
    uint16_t lastAdr=Sram_GetLastActionAddress();
    SRAM_CS_PORT=0;
    
    uint8_t rawBuf[9]={0};
    uint16_t resBuf[6]={0};
    uint8_t cnt=0;
    
    uint16_t sampleCnt=0;
    uint8_t txCnt=0;
    uint8_t txSum=zigBeeSendTemplateDefaultSum;
    
    SendRequest();
    
    uint16_t startAdr=lastAdr;
    if(lastAdr<PACKET_NUM*PACKET_BYTE){
        uint16_t adtmp=lastAdr;
        startAdr=0xFFF8;
        startAdr-=(PACKET_NUM*PACKET_BYTE-lastAdr);
    } else {
        startAdr -= PACKET_NUM*PACKET_BYTE;
    }
    startAdr++;
    if (Sram_GetLastActionAddress()==0xFFF8){
        startAdr=0xFFFF;
    }
    Sram_ReadByte(startAdr);
    printf("s: %04X\te: %04X\n\r",startAdr,lastAdr);

    if (Sram_GetLastActionAddress()==0xFFF8){
        SRAM_CS_PORT=1;
        for(uint8_t i=0;i<10;i++);
        SRAM_CS_PORT=0;
        Sram_ReadByte(0xFFFF);
    }
    printf("start at %04x\n\r",Sram_GetLastActionAddress());

    
    while(1){
        if(rxFlag=='r'){
            rxFlag=0;
            break;
        }
        uint8_t readBuf=Sram_ReadByteNextAddress();
        txDat[txCnt+2]=readBuf;
        txCnt++;
        if(txCnt>=PACKET_BYTE){
            
            ZIGBEE_SEND:
            for(uint8_t i=0;i<17;i++){
                EUSART2_Write(zigBeeSendTemplate[i]);
            }
            txDat[0]=(uint8_t)((sampleCnt&0xFF00)>>8);
            txDat[1]=(uint8_t)(sampleCnt&0x00FF);
            
            for(uint8_t i=0;i<PACKET_BYTE+2;i++){
                txSum+=txDat[i];
                if(txDat[i]==0x11 ||
                   txDat[i]==0x13 ||
                   txDat[i]==0x7D ||
                   txDat[i]==0x7E){
                   EUSART2_Write(0x7D);
                   EUSART2_Write(txDat[i]^0x20);
                   continue;
                }
                EUSART2_Write(txDat[i]);
            }
            
            txSum=0xFF-txSum;
            zigBeeTxError=0xFF;
            if(txSum==0x11 ||
                txSum==0x13 ||
                txSum==0x7D ||
                txSum==0x7E){
                EUSART2_Write(0x7D);
                EUSART2_Write(txSum^0x20);
            } else {
                EUSART2_Write(txSum);
            }
            printf("adr:%04X wait...\t",_current_addr);
            tmptimer=0;
            while(1){
                //uint8_t errorFlag=0;
                tmptimer++;
                if(tmptimer>=50000){
                    printf("retry\n\r");
                    goto ZIGBEE_SEND;
                }
                if(zigBeeTxError!=0xFF){
                    if(zigBeeTxError==0x00){
                        //printf("");
                        break;
                    } else if(zigBeeTxError!=0xFE){
                        printf("error %02x    ",zigBeeTxError);
                        zigBeeTxError=0xFE;
                    }
                }
                __delay_us(10);
            }
            printf("success  time: %04X   no: %04X \tcode: %02X\n\r",tmptimer, sampleCnt,zigBeeTxError);
            zigBeeTxError=0xFF;
            txCnt=0;
            txSum=zigBeeSendTemplateDefaultSum;
            sampleCnt++;
        }
        
        if (Sram_GetLastActionAddress()==0xFFF8){
            SRAM_CS_PORT=1;
            for(uint8_t i;i<10;i++);
            SRAM_CS_PORT=0;
            Sram_ReadByte(0xFFFF);
        }
        if (Sram_GetLastActionAddress() == lastAdr ){
            printf("read finish\n\r");
            SRAM_CS_PORT=1;
            rxFlag=0;
            SendFinish();
            break;
        }
    }
    XBEE_SLEEP_SetHigh();
    isAssociated=0;
    TMR1_StartTimer();
}

/**
 End of File
*/