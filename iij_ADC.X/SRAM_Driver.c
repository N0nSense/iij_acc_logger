
#include "SRAM_Driver.h"

volatile uint16_t _current_addr=0;


void Sram_SetSpiExchangeByteFunction( uint8_t(*func)(uint8_t) ){
    SpiExchangeByte=func;
    return;
}

uint8_t Sram_Init(){
    if(SpiExchangeByte==0){
        return 1;
    }
    SpiExchangeByte(0x05);
    SpiExchangeByte(0x40);
    SpiExchangeByte(0x01);
    SpiExchangeByte(0x40);
    return 0;
}

uint8_t Sram_Init1(){
    if(SpiExchangeByte==0){
        return 1;
    }
    SpiExchangeByte(0x05);
    SpiExchangeByte(0x40);
    return 0;
}

uint8_t Sram_Init2(){
    if(SpiExchangeByte==0){
        return 1;
    }
    SpiExchangeByte(0x01);
    SpiExchangeByte(0x40);
    return 0;
}

uint16_t Sram_GetLastActionAddress(){
    return _current_addr;
}


void Sram_SetAddress(uint16_t addr){
    _current_addr=addr;
    return;
}

uint8_t Sram_ReadByte(uint16_t addr){
    SpiExchangeByte(0x03);          // instruction : Read
    uint8_t tmp=0;
    tmp=addr>>8;
    SpiExchangeByte(tmp);           // High Address
    tmp=addr&0x00FF;
    SpiExchangeByte(tmp);           // Low Address
    _current_addr= addr;
    return SpiExchangeByte(0x00);   // Data Out
}

void Sram_Read(uint8_t *buf, uint16_t len, uint16_t addr){
    *buf=Sram_ReadByte(addr);
    while(--len != 0){
        *++buf =SpiExchangeByte(0x00);
        _current_addr=++addr;
    }
    return;
}

uint8_t Sram_ReadByteNextAddress(){
    _current_addr++;
    return SpiExchangeByte(0x00);
}


void Sram_WriteByte(uint8_t value, uint16_t addr){
    SpiExchangeByte(0x02);  // instruction : Write
    uint8_t tmp=0;
    tmp=addr>>8;
    SpiExchangeByte(tmp);   // High Address
    tmp=addr&0x00FF;
    SpiExchangeByte(tmp);   // Low Address
    _current_addr= addr;
    SpiExchangeByte(value); // Data Write
    return;
}

void Sram_WriteByteNextAddress(uint8_t value){
    SpiExchangeByte(value);
    _current_addr++;
    return;
}



void Sram_Write(uint8_t *buf, uint16_t len, uint16_t addr){
    
    Sram_WriteByte(*buf, addr);
    while(--len != 0){
        SpiExchangeByte(*++buf);
        _current_addr=++addr;
    }
    return;
}
